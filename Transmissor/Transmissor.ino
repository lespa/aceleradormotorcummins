/*  
***** TRANSMISSOR *****

Aplicação: Este programa envia duas variáveis inteiras
           recebidas em duas entradas analógicas 
           e envia via rádio usando um módulo nRF24L01+
           

NRF  |  ARDUINO
Vcc  => + 3.3v
GND  => GND
CE   => PIN 9 
CSN  => PIN 10 - SS
SCK  => PIN 13 - SCK
MO   => PIN 11 - MOSI
MI   => PIN 12 - MISO
IRQ  => SEM USO
*/

/* Bibliotecas */
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

int analogico[2]; // Matriz do tipo int chamada analogico

RF24 radio(9,10);  // Cria um objeto chama radio conectado nos pinos 9 e 10 do Arduino

const uint64_t pipe = 0xE8E8F0F0E255;//LL; //Configurando endereço de comunicação entre os MÓDULOS,

void setup(void)
{ 
  //Serial.begin(9600);          // Inicia a Serial
  radio.begin();               // Inicia o modulo para comunicação.
  
  radio.setChannel(5);         // Set canal de comunicacao
  
  radio.openWritingPipe(pipe); // abrindo o meio de comunicação (Tubo), com
                               // o endeço definido no ínicio do programa
  
  //radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
}

void loop()
{
  analogico[0] = analogRead(A6); // Faz a leitura analogica no pino A0
  //analogico[0] = 1000; // Para teste
  analogico[1] = analogRead(A7); // Faz a leitura analogica no pino A1
 
 //analogico[0] = 12345; // Faz a leitura analogica no pino A0
 //analogico[1] = 54321;
 
      //Serial.print("Transmissor - Lido em A0:  ");
      //Serial.print(analogico[0]);
      //Serial.print(" ");
      //Serial.print("  Lido em A1:  ");
      //Serial.println(analogico[1]);
      //Serial.println("");  
 
 radio.write( analogico, sizeof(analogico) ); // Envia a Matriz analogico

 delay(5);
}



