/*  
***** RECEPTOR *****

Aplicação: Este código tem por obejtvo estabelecer a comunicação
           entre 2 arduinos através do modulo RF NRF24L01+
           
           
**** CONEXÕES MODULO PARA ARDUINO ****

NRF  |  ARDUINO
Vcc  => + 3.3v
GND  => GND
CE   => PIN 9 
CSN  => PIN 10 - SS
SCK  => PIN 13 - SCK
MO   => PIN 11 - MOSI
MI   => PIN 12 - MISO
IRQ  => SEM USO
*/

/** Bibliotecas **/
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
 
int analogico[2]; // Matriz do tipo int chamada analogico

int pinoAcelerador = 3; // Saida PWM para controlar o motor

int pinoSinal = 4; // Pino indicando se esta recebendo dados

int estadoLed = 0;

int limiteEstadoLed = 100;  // Numero de falhas antes de apagar o led de sinal

float valorPWM1 = 0.0; // Valor setado na saida PWM de aceleracao

float valorPWM2 = 0.0; // Valor setado na saida PWM de aceleracao

float valorPwmAnterior1 = 0.00; // Valor setados no ultimo ciclo
float valorPwmAnterior2 = 0.00; // Valor setados no ultimo ciclo

RF24 radio(9,10); // Cria um objeto chama radio conectado nos pinos 9 e 10 do Arduino
                  
/* Configura o endereço de comunicação entre os Módulos */                  
const uint64_t pipe = 0xE8E8F0F0E255;//LL; // Tubo de comunicação

void setup(void)
{
  //Serial.begin(9600);            // Inicia a Serial
  radio.begin();                 // Inicia o modulo para comunicação.

  radio.setChannel(5);           // Set canal de comunicacao
  
  radio.openReadingPipe(1,pipe); // abrindo o meio de comunicação (Tubo), com
                                 // o endeço definido no ínicio do programa
  radio.startListening();        // Inicia o modulo para ouvir as requisições.
  
  pinMode(pinoAcelerador, OUTPUT); // Set pino como saida
  
  pinMode(pinoSinal, OUTPUT); // Set pino como saida
  
  //Serial.println("Sou o receptor");
}

void loop()
{
  float pwmSet1 = 0.0;
  float pwmSet2 = 0.0;
  
  /* verifica se o radio ouviu */
  if ( radio.available() )
  {
    bool done = false;
    while (!done)
    {
      done = radio.read( analogico, sizeof(analogico) );
    }
    
      /* Mostra na Serial o Valor de A0 e A1 enviado pelo trasmissor */
    //Serial.print("Receptor - A0 Recebido:  ");
    //Serial.print(analogico[0]);
    //Serial.print("  ");
    //Serial.print("  A1 recebido:  ");
    //Serial.println(analogico[1]);
    //Serial.println("");
    
    valorPWM1 = calcValorPwm1(analogico[0]);
    //Serial.print("PWM1: ");
    //Serial.println(valorPWM1);
    
    valorPWM2 = calcValorPwm2(analogico[1]);
    //Serial.print("PWM2: ");
    //Serial.println(valorPWM2);
    
    if(valorPWM1 != -1){
      pwmSet1 = 255 - valorPWM1;
    
      if(pwmSet1 < 90 && valorPwmAnterior1 > 90)
        pwmSet1 = valorPwmAnterior1 - 5;
    }
    
    if(valorPWM2 != -1){
      pwmSet2 = 255 - valorPWM2;
      
      if(pwmSet2 < 90 && valorPwmAnterior2 > 90)
        pwmSet2 = valorPwmAnterior2 - 5;
    }
    
      //Serial.print("pwmSet11-------");
      //Serial.println(pwmSet1);
      
      //Serial.print("pwmSet22-------");
      //Serial.println(pwmSet2);
    
    if(valorPWM1 != -1 && pwmSet1 > 90){
      analogWrite(pinoAcelerador, pwmSet1);
      valorPwmAnterior1 = pwmSet1;
      //Serial.print("pwmSet   ");
      //Serial.println(pwmSet1);
    }
    if(valorPWM2 != -1 && pwmSet2 > 90){
      analogWrite(pinoAcelerador, pwmSet2);
      valorPwmAnterior2 = pwmSet2;
      //Serial.print("pwmSet   ");
      //Serial.println(pwmSet2);
    }
    
    digitalWrite(pinoSinal, HIGH);         // set led on
  }
  else
  {
    //Serial.println("Nenhum dado recebido");
    analogWrite(pinoAcelerador, 0);
    
    estadoLed--;
    if(estadoLed <= 0){
      analogWrite(pinoAcelerador, 0);
      digitalWrite(pinoSinal, LOW);          // set led off
      estadoLed = limiteEstadoLed;
    }
  }
  
  delay(5); // Delay para atualização
}

/* Convertendo 340<->840 para 135<->255 para enviar para a saida PWM*/
float calcValorPwm1(int valor)
{  
    float difAnalogico = 0.0; // Valor subitraindo valor recebido por 170

    difAnalogico = (float)(valor);
    
    if(difAnalogico < 100 || difAnalogico > 1000)
      return -1;

    difAnalogico -= 340;
    
    if(difAnalogico < 0)
      return 0;
    else if(difAnalogico > 500) //840 - 340 = 500
      return 255;
    else {
      return (float)(difAnalogico)*0.33; //((255-90)/500)
    }
}

/* Convertendo 230-720 para 135-255 para enviar para a saida PWM*/
float calcValorPwm2(int valor)
{
    float difAnalogico = 0.0; // Valor subitraindo valor recebido por 170

    difAnalogico = (float)(valor);
    
    if(difAnalogico < 100 || difAnalogico > 1000)
      return -1;

    difAnalogico -= 230;

    if(difAnalogico < 0)
      return 0;
    else if(difAnalogico > 490) //720 - 230
      return 255;
    else {
      return (float)(difAnalogico)*0.3367; //((255-90)/490)
    }
}
