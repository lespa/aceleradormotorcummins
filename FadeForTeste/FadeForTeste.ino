int led1 = 6;           // the pin that the LED is attached to
int led2 = 5;           // the pin that the LED is attached to

int brightness = 0;    // how bright the LED is
int fadeAmount = 3;    // how many points to fade the LED by

// the setup routine runs once when you press reset:
void setup()  {
  //Serial.begin(9600);
  // declare pin 9 to be an output:
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
} 

// the loop routine runs over and over again forever:
void loop()  {
  analogWrite(led1, brightness);
  analogWrite(led2, brightness);
  
  //Serial.println(analogRead(A0));

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade: 
  if (brightness == 0 || brightness == 255) {
    fadeAmount = -fadeAmount ; 
  }     
  // wait for 30 milliseconds to see the dimming effect    
  delay(50);                            
}



